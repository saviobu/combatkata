﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting;

namespace Combat_Kata_ns
{
    public class Objeto : Avatar 
    {
        public Objeto(int health,string nome, int posx, int posy)
        {
            alive = true;
            max = health;
            this.health = health;
            this.nome = nome;
            tipo = "objeto";
            posicao[0] = posx;
            posicao[1] = posy;
        }
    }
    public class Personagem : Avatar
    {
        public Personagem(string tipo,string nome)
        {
            health = max;
            level = 1;
            alive = true;
            faccao = new List<string>();
            this.tipo = tipo;
            this.nome = nome;
        }
    }
    public class Avatar : IDisposable //agregamos a classe Objeto
    {
        public int max=1000;

        private bool disposedValue;
        public int health { get; set; }
        public string nome { get; set; }
        public bool alive { get; set; }
        public int[] posicao { get; set; } = {0,0};
        public int level { get; set; }
        public string tipo { get; set; }
        public List<string> faccao { get; set; }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // Tarefa pendente: descartar o estado gerenciado (objetos gerenciados)
                }
                disposedValue = true;
            }
        }
        void IDisposable.Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    public class Acoes
    {
        public static bool Heal(int hl, Avatar avatar)
        {
            if (avatar.alive)
            {
                avatar.health += hl;
                if (avatar.health > avatar.max) avatar.health = avatar.max;
            }
            if (avatar.health > avatar.max) avatar.health = avatar.max;
            return true;
        }
        public static bool Damage(int dmg, Avatar avatar)
        {
            avatar.health -= dmg;

            if (avatar.health < 0)
            {
                avatar.health = 0;
                avatar.alive = false;
                avatar.Dispose();
            }
            return true;
        }
    }
    public class Desloca_Mapa //vamos limitar em 25x25 posicoes ( metros )
    {
        public static bool Acima(Avatar avatar)
        {
            if (avatar.posicao[1] < 25) avatar.posicao[1]++;
            return true;
        }
        public static bool Abaixo(Avatar avatar)
        {
            if (avatar.posicao[1] > 0) avatar.posicao[1]--;
            return true;
        }
        public static bool Direita(Avatar avatar)
        {
            if (avatar.posicao[0] < 25) avatar.posicao[0]++;
            return true;
        }
        public static bool Esquerda(Avatar avatar)
        {
            if (avatar.posicao[0] > 0) avatar.posicao[0]--;
            return true;
        }
    }
    public class Disputa : Acoes
    {
        public static bool Atacar(Avatar atacante, Avatar vitima, int damage)
        {
            if (vitima.health>0)
            {
                if (atacante != vitima && atacante.tipo != "objeto")
                {
                    if (vitima.tipo != "objeto")
                    {
                        if ((atacante.tipo == "Melee" && (Math.Abs(atacante.posicao[0] - vitima.posicao[0]) <= 2)) &&
                            (atacante.tipo == "Melee" && (Math.Abs(atacante.posicao[1] - vitima.posicao[1]) <= 2)) ||
                            (atacante.tipo == "Ranged" && (Math.Abs(atacante.posicao[0] - vitima.posicao[0]) <= 20)) &&
                            (atacante.tipo == "Ranged" && (Math.Abs(atacante.posicao[1] - vitima.posicao[1]) <= 20)))
                        {
                            if (atacante.level >= (vitima.level + 5)) damage = (int)((float)damage * 1.5);
                            if (vitima.level >= (atacante.level + 5)) damage = (int)((float)damage * 0.5);

                            if (atacante.faccao.Count > 0 && vitima.faccao.Count > 0)
                            {
                                if (!atacante.faccao.Intersect(vitima.faccao).Any()) Acoes.Damage(damage, vitima);
                            }
                            else Acoes.Damage(damage, vitima);
                        }
                    }
                    else Acoes.Damage(damage, vitima);
                }
            }
            return true;
        } 
        public static bool Curar(Avatar curador, Avatar recebedor, int heal)
        {
            if (curador == null) Acoes.Heal(heal, recebedor);
            else
            {
                if (recebedor.tipo != "objeto" && curador.tipo != "objeto")
                {
                    if (recebedor.faccao.Count > 0 && curador.faccao.Count > 0)
                    {
                        if (recebedor.faccao.Intersect(curador.faccao).Any()) Acoes.Heal(heal, recebedor);
                    }
                    else
                    {
                        if (recebedor.faccao.Count == 0 && curador.faccao.Count == 0) Acoes.Heal(heal, recebedor);
                    }
                }
            }   
            return true;
        }
    }
    public class Faccoes : Avatar
    {
        public static bool Entrar(Avatar avatar, string[] faccao_gp)
        {
            if (avatar.tipo!="objeto")
            {
                foreach (string faccao in faccao_gp)
                {
                    if (!avatar.faccao.Contains(faccao)) avatar.faccao.Add(faccao);
                }
            }
            return true;
        }
        public static bool Sair(Avatar avatar, string[] faccao_gp)
        {
            if (avatar.tipo != "objeto")
            {
                foreach (string faccao in faccao_gp)
                {
                    if (avatar.faccao.Contains(faccao)) avatar.faccao.Remove(faccao);
                }
            }
            return true;
        }
    }
}