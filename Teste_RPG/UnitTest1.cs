using Microsoft.VisualStudio.TestTools.UnitTesting;
using Combat_Kata_ns;
using System.Runtime.InteropServices.ComTypes;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Teste_RPG
{
    [TestClass]
    public class UnitTest1
    {
        public List<Personagem> per;
        public List<Objeto> obj;

        [TestMethod]
        public void Criar_Avatar_e_Objeto()
        {
            string[] tipo = {"Melee", "Ranged", "Ranged", "Ranged", "Melee" , "Ranged", "Melee" };
            string[] nome = {"Tomas","Pedro","Alberto","Tiago","Joao", "Jose","Ana"};
            string[] nome_objeto = { "Parede_Tijolo","Parede_Madeira","Cadeira", "Mesa", "Pedra", "Parede_Dryall" };
            int[] health_objeto = { 4000 , 2500 , 1000 , 1200 , 3000 , 200 };
            int[] px = {2 , 3 , 5 , 1 , 15 , 10};
            int[] py = {4 , 8 , 7 , 6 , 20 , 25 };
            per = new List<Personagem>();
            obj = new List<Objeto>();

            int cont = 0;

            foreach (string tp in tipo)
            {
                per.Add(new Personagem(tp, nome[cont]));
                cont++;
            }
            
            cont = 0;

            foreach (int ho in health_objeto)
            {
                obj.Add(new Objeto(ho, nome_objeto[cont],px[cont],py[cont]));
                cont++;
            }
        }

        [TestMethod]
        public void Testa_Limites_Deslocamento()
        {
            var vezes = 30;

            var per = new Personagem("", "");
            
            int cont = 0;

            while (cont < vezes)
            {
                Desloca_Mapa.Acima(per);
                Desloca_Mapa.Direita(per);
                cont++;
            }

            Assert.AreEqual(per.posicao[0], 25);
            Assert.AreEqual(per.posicao[1], 25);
            
            while (cont != -vezes)
            {
                Desloca_Mapa.Abaixo(per);
                Desloca_Mapa.Esquerda(per);
                cont--;
            }

            Assert.AreEqual(per.posicao[0], 0);
            Assert.AreEqual(per.posicao[1], 0);
        }
        
        [TestMethod]
        public void Testar_Acoes_Heal_Damage()
        {
            Criar_Avatar_e_Objeto();

            Disputa.Atacar(obj[0],per[0],100); //objeto atacando personagem
            Assert.AreEqual(per[0].health, 1000);

            Disputa.Atacar(per[1], per[0], 100); //personagem atacando personagem distinto
            Assert.AreEqual(per[0].health, 900);

            Disputa.Atacar(per[2], per[2], 100);//personagem atacando ele mesmo
            Assert.AreEqual(per[2].health, 1000);

            Disputa.Atacar(per[1], per[2], 200); //per[2] 800
            Disputa.Atacar(per[2], per[1], 500); //per[1] 500
            Disputa.Atacar(per[3], per[2], 400); //per[2] 400

            Disputa.Curar(obj[0], per[0], 100); //objeto curando personagem
            Assert.AreEqual(per[0].health, 900);

            Disputa.Curar(per[0], obj[1], 100); //personagem curando objeto
            Assert.AreEqual(obj[1].health, 2500);

            Disputa.Curar(per[1], per[0], 100); //personagem curando personagem distinto
            Assert.AreEqual(per[0].health, 1000);

            Disputa.Curar(per[1], per[0], 100); //novamente curando tentando exceder 1000 de health
            Assert.AreEqual(per[0].health, 1000);

            Disputa.Curar(null,per[2], 100);//personagem curando ele mesmo
            Assert.AreEqual(per[2].health, 500);

            Disputa.Atacar(per[2],per[0],1100); //matar personagem
            Assert.AreEqual(per[0].health, 0);
            Assert.AreEqual(per[0].alive, false);

            Disputa.Curar(per[2], per[0], 500); //tentando curar um morto
            Assert.AreEqual(per[0].health, 0);

            Disputa.Atacar(per[2], obj[0], 4100); //destruir objeto
            Assert.AreEqual(obj[0].health, 0);
            Assert.AreEqual(obj[0].alive, false);

            per[5].level = 6;
            Disputa.Atacar(per[4], per[5], 200); //Vitima mais forte que o atacante
            Assert.AreEqual(per[5].health, 900);

            per[4].level = 12;
            Disputa.Atacar(per[4], per[5], 200); //Atacante mais forte que a vitma
            Assert.AreEqual(per[5].health, 600);

            per[5].posicao[0] = 3;
            per[5].posicao[1] = 0;
            Disputa.Atacar(per[4], per[5], 100); //Atacante Melee fora de alcance X
            Assert.AreEqual(per[5].health, 600);

            per[5].posicao[0] = 0;
            per[5].posicao[1] = 3;
            Disputa.Atacar(per[4], per[5], 100); //Atacante Melee fora de alcance y
            Assert.AreEqual(per[5].health, 600);

            per[5].posicao[0] = 2;
            per[5].posicao[1] = 2;
            Disputa.Atacar(per[4], per[5], 100); //Atacante Melee dentro do alcance
            Assert.AreEqual(per[5].health, 450);

            per[5].posicao[0] = 21;
            per[5].posicao[1] = 0;
            Disputa.Atacar(per[3], per[5], 100); //Atacante Ranged fora de alcance X
            Assert.AreEqual(per[5].health, 450);

            per[5].posicao[0] = 0;
            per[5].posicao[1] = 21;
            Disputa.Atacar(per[3], per[5], 100); //Atacante Ranged fora de alcance y
            Assert.AreEqual(per[5].health, 450);

            per[5].posicao[0] = 17;
            per[5].posicao[1] = 11;
            Disputa.Atacar(per[3], per[5], 100); //Atacante Ranged dentro do alcance
            Assert.AreEqual(per[5].health, 400);
        }

        [TestMethod]
        public void Testar_Faccoes_Entrar_Sair()
        {
            Criar_Avatar_e_Objeto();
            
            string[] var_entra = { "Piratas" };
            Faccoes.Entrar(per[0], var_entra); //entrou em apenas uma faccao
            var ret = "Piratas";
            var val = string.Join('.',per[0].faccao);
            Assert.AreEqual(val,ret); 

            var_entra[0] = "Justiceiros";
            Faccoes.Entrar(per[0], var_entra); // entrou em mais uma
            var_entra[0] = "Piratas";
            Faccoes.Entrar(per[0], var_entra); //faccao repetida
            ret = "Piratas.Justiceiros";
            val = string.Join('.', per[0].faccao);
            Assert.AreEqual(val, ret);
            
            string[] var2_entra = { "Piratas", "Ferreiros", "Orcs", "Arqueiros" };
            Faccoes.Entrar(per[0], var2_entra);//entrou em varias faccoes
            ret = "Piratas.Justiceiros.Ferreiros.Orcs.Arqueiros";
            val = string.Join('.', per[0].faccao);
            Assert.AreEqual(val, ret);

            string[] var_sai = { "Piratas" };
            Faccoes.Sair(per[0], var_sai); //sai de apenas uma faccao
            ret = "Justiceiros.Ferreiros.Orcs.Arqueiros";
            val = string.Join('.', per[0].faccao);
            Assert.AreEqual(val, ret);

            var_sai[0] = "Justiceiros";
            Faccoes.Sair(per[0], var_sai); // saiu de mais uma
            var_sai[0] = "Piratas";
            Faccoes.Sair(per[0], var_sai); //sai de faccao repetida, ja havia saido dela
            ret = "Ferreiros.Orcs.Arqueiros";
            val = string.Join('.', per[0].faccao);
            Assert.AreEqual(val, ret);

            string[] var2_sai = { "Orcs", "Arqueiros" };
            Faccoes.Sair(per[0], var2_sai);//sai de varias faccoes
            ret = "Ferreiros";
            val = string.Join('.', per[0].faccao);
            Assert.AreEqual(val, ret);
        }
        [TestMethod]
        public void Testar_Faccoes_Damage_Heal()
        {
            Criar_Avatar_e_Objeto();

            string[] var_entra = { "Piratas" };
            Faccoes.Entrar(per[0], var_entra); //entrou em uma faccao
            var ret = "Piratas";
            var val = string.Join('.', per[0].faccao);
            Assert.AreEqual(val, ret);

            Faccoes.Entrar(per[1], var_entra); //entrou na mesma faccao
            ret = "Piratas";
            val = string.Join('.', per[1].faccao);
            Assert.AreEqual(val, ret);

            var_entra[0] = "Guerreiros" ;
            Faccoes.Entrar(per[1], var_entra); //entrou em outra faccao
            ret = "Piratas.Guerreiros";
            val = string.Join('.', per[1].faccao);
            Assert.AreEqual(val, ret);

            Disputa.Atacar(per[0], per[1], 100); //Atacando Aliado ( pelo menos uma faccao em comum )
            Assert.AreEqual(per[1].health, 1000);

            Disputa.Atacar(per[3], per[1], 100); //Atacando inimigo
            Assert.AreEqual(per[1].health, 900);

            Disputa.Curar(per[5], per[1], 100); //Inimigos se curando
            Assert.AreEqual(per[1].health, 900);

            Disputa.Curar(per[0], per[1], 100); //Aliados se curando
            Assert.AreEqual(per[1].health, 1000);
        }
    }
}